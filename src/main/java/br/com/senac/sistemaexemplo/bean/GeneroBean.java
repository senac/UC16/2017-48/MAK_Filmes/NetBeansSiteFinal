/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.sistemaexemplo.bean;

import br.com.senac.sistemaexemplo.dao.GeneroDAO;
import br.com.senac.sistemaexemplo.model.Genero;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import org.primefaces.model.DualListModel;

@Named(value = "generoBean")
@ViewScoped
public class GeneroBean  extends Bean{

    private Genero genero;
    private GeneroDAO dao ; 
    private List<Genero> generos ;
    
  

    public GeneroBean() {
       
    }


    @PostConstruct
    public void init() {
        this.genero = new Genero();
        this.dao = new GeneroDAO();
        this.generos = dao.findAll();
        
     
    }
    
    public void salvar(){
         if(this.genero.getId() == 0 ){
            dao.save(genero);
            addMessageInfo("Salvo com sucesso.");
        }else{
            dao.update(genero);
            addMessageInfo("Atualizado com sucesso.");
        }
         this.generos = dao.findAll();
    }
    
    public void novo(){
        this.genero = new Genero();
    }
    
    public void excluir(){
       
        
    }
    
    

    public Genero getGenero() {
        return genero;
    }

    public void setGenero(Genero genero) {
        this.genero = genero;
    }

    public List<Genero> getGeneros() {
        return generos;
    }

    public void setGeneros(List<Genero> generos) {
        this.generos = generos;
    }

    
    
}
