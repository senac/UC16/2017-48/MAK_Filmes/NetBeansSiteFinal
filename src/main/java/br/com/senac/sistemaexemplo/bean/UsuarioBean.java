package br.com.senac.sistemaexemplo.bean;

import br.com.senac.sistemaexemplo.dao.UsuarioDAO;
import br.com.senac.sistemaexemplo.model.Usuario;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

@Named(value = "usuarioBean")
@ViewScoped
public class UsuarioBean extends Bean {

    private Usuario usuario;
    private UsuarioDAO dao;
    private List<Usuario> listaUsuarios ; 

    public UsuarioBean() {

    }

    @PostConstruct
    public void init() {
        this.usuario = new Usuario();
        this.dao = new UsuarioDAO();
        this.listaUsuarios = dao.findAll() ; 
    }
    
    
    public void salvar(){
        
        if(this.usuario.getId() == 0 ){
            dao.save(usuario);
            addMessageInfo("Salvo com sucesso.");
        }else{
            dao.update(usuario);
            addMessageInfo("Atualizado com sucesso.");
        }
         this.listaUsuarios = dao.findAll() ; 
    }
    
    public void novo(){
        this.usuario = new Usuario();
    }
    
    public void deletar(Usuario usuario){
        this.dao.delete(usuario);
        addMessageInfo("Usuario deletado com sucesso.");
        this.listaUsuarios = dao.findAll();
    }
    
    

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public List<Usuario> getListaUsuarios() {
        return listaUsuarios;
    }

    public void setListaUsuarios(List<Usuario> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }
    
    
    
    

}
