/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.sistemaexemplo.bean;

import br.com.senac.sistemaexemplo.dao.GeneroDAO;
import br.com.senac.sistemaexemplo.model.Genero;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import org.primefaces.model.DualListModel;

/**
 *
 * @author sala304b
 */
@Named(value = "filmeBean")
@ViewScoped
public class FilmeBean extends Bean{

    private DualListModel<Genero> listaGenero;
    private GeneroDAO dao;

    public FilmeBean() {
    }

    @PostConstruct
    public void init() {

        this.dao = new GeneroDAO();
        
        //Themes
        List<Genero> themesSource = dao.findAll();
        List<Genero> themesTarget = new ArrayList<Genero>();

        listaGenero = new DualListModel<Genero>(themesSource, themesTarget);
        
        
    }

    public DualListModel<Genero> getListaGenero() {
        return listaGenero;
    }

    public void setListaGenero(DualListModel<Genero> listaGenero) {
        this.listaGenero = listaGenero;
    }

}
