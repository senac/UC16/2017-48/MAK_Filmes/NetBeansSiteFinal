
package br.com.senac.sistemaexemplo.dao;

import br.com.senac.sistemaexemplo.model.Genero;


public class GeneroDAO extends DAO<Genero>{
    
    public GeneroDAO() {
        super(Genero.class);
    }
    
}
