
package br.com.senac.sistemaexemplo.bean;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

public abstract class Bean implements Serializable{
    
    public void addMessageInfo(String mensagem) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage("Sucesso",
                new FacesMessage(FacesMessage.SEVERITY_INFO, mensagem, "Sucesso"));

    }

    public void addMessageError(String mensagem) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage("Erro",
                new FacesMessage(FacesMessage.SEVERITY_ERROR, mensagem, "Erro"));

    }

    public void addMessageWarnnig(String mensagem) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage("Aviso",
                new FacesMessage(FacesMessage.SEVERITY_WARN, mensagem, "Aviso"));

    }
}
